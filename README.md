# `Projet Blog`
Création d'une plateforme de blog en node/react avec une gestion des utilisateur·ices.

## `Fonctionnalités`

Un user doit pouvoir s'inscrire, se connecter et une fois connecter doit pouvoir poster des articles sur son espace personnel.
N'importe qui peut consulter les articles des autres user.


### `Réalisation`

J'ai d'abord realiser un diagramme de Use Case pour identifierles différentes fonctionnalités sur le blog

<img src="public/images/usecase-blog.png" width="300" height="300" />


J'ai ensuite créer une maquette fonctionnelle du blog

<img src="public/images/login.png" width="500" height="600" />
<br><hr>
<img src="public/images/home.png" width="500" height="600" />
<br><hr>
<img src="public/images/home-mobile.png" width="400" height="700" />
<br><hr>

Puis j'ai identifé les entités qui persisteront en bdd à l'aide d'un un diagramme de classe

<img src="public/images/diagramme-classe-blog.png" width="300" height="300" />

Puis j ' ai decider de commencer par le front en créant une interface avec React
Ensuite j' ai créer une API avec node/express
J'ai réaliser mon identification une authentification avec hashing et JWT


### `Liens`


----------------- PROJETS -----------------
- [ Projet en ligne ](https://kamel-ben-abdelkader.gitlab.io/app-blog-client/)
- Front : [ Blog client ](https://gitlab.com/kamel-ben-abdelkader/app-blog-client)
- Back() : [Blog serveur](https://gitlab.com/kamel-ben-abdelkader/app-blog-serveur)



-----------------

- 👨‍💻 Tous mes projets en cours sont disponibles sur mon GITLAB : [GITLAB ](https://gitlab.com/kamel-ben-abdelkader)


-----------------

<h3>Me contacter:</h3>
<p align="left">
<a href="https://linkedin.com/in/https://www.linkedin.com/in/kamel-ben-abdelkader-094928167/" target="blank"><img align="center" src="public/images/maquette/linkedin.png" alt="https://www.linkedin.com/in/kamel-ben-abdelkader-094928167/" height="30" width="40" /></a>
</p>



<h3>Language et outil:</h3>

<h6>Front :</h6> 

- HTML
- CSS
- React
- Axios
- React
- Material UI

-----------------


<h6>Back :</h6> 

- Mysql2
- NodeJS
- Bcrypt
- Cors
- express
- Dotenv-flow
- Passport-jwt
- Multer
- Joi
