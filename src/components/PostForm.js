import { useState } from "react";
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Link, useHistory } from "react-router-dom";
import { Button, Hidden } from "@material-ui/core";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';



// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Projet Blog
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 124, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));




export function PostForm({ onSubmit }) {
  const classes = useStyles();
  let history = useHistory();
  function handleClick() {
    history.push("/");
  }


  const [form, setForm] = useState([]);

  const handleChange = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    });
  }

  const handleFile = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.files[0]
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit(form);
    handleClick()
  }

  return (
<>
    <AppBar position="absolute" >
    <Toolbar className={classes.toolbar}>
    <Hidden  xsDown>
      <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
     AJOUT NOUVEL ARTICLE

      </Typography>
      </Hidden>
      <IconButton color="inherit">
        <Badge color="secondary">
        <Link to="/homeboard"><Button variant="outlined" size="large">Dashboard</Button></Link>

         
        </Badge>
      </IconButton>
    </Toolbar>
  </AppBar>


    <form className = 'formAdd' onSubmit={handleSubmit}>


      <TextField

        id="date"
        label="Date"
        type="date"
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        name="date"
        onChange={handleChange}
        value={form.date}
      />

      <TextField
        required
        fullWidth
        label="Auteur"
        id="outlined-margin-normal"
        defaultValue="Indiquer l'auteur"
        className={classes.textField}
        helperText="Indiquer l'auteur' de votre Article"
        margin="normal"
        variant="outlined"
        type="text"
        name="auteur"
        // value={form.auteur}
        onChange={handleChange}
      />

      <TextField
        fullWidth
        required
        label="Titre"
        id="outlined-margin-normal"
        defaultValue="Indiquer votre Titre"
        className={classes.textField}
        helperText="Indiquer le titre de votre Article"
        margin="normal"
        variant="outlined"
        type="text"
        name="titre"
        value={form.titre}
        onChange={handleChange}
      />
      <h5>Ajouter une photo</h5>
      <input required className="ButtonForm" placeholder="picture" type="file" name="picture" onChange={handleFile} />



      <TextField
        required
        label="Article"
        id="outlined-margin-normal"
        defaultValue="Rediger votre Article"
        className={classes.textField}
        helperText="Indiquer le texte de votre Article"
        margin="normal"
        variant="outlined"
        type="text"
        name="texte"
        multiline
        fullWidth
        MuiInput-marginDense
        rows={15}
        value={form.texte}
        onChange={handleChange}
      />
      <button className="ButtonForm">Publier</button>
    </form>
    </>
  );
}