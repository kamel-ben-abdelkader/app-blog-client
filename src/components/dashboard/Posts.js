import React, { useEffect } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import { deletePosts, fetchPostsByUser, setListUser, updatePosts } from '../../stores/post-slice';
import { useDispatch, useSelector } from 'react-redux';
import { Button, CardActions } from '@material-ui/core';




const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Posts() {

  const classes = useStyles();



  const user = useSelector(state => state.auth.user);



  const dispatch = useDispatch()
  const posts = useSelector(state => state.posts.listUser);

  const onDelete = (id) => {
    dispatch(setListUser(posts.filter(item => item.id !== id)))
    dispatch(deletePosts(id));


  }



  // const update= (id) => {
  //   dispatch(updatePosts(id));


  // }

  useEffect(() => {

    dispatch(fetchPostsByUser(user.id));
  }, [dispatch]);


  return (
    <React.Fragment>
      <Title>Recent Posts</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Titre</TableCell>
            <TableCell>Extrait</TableCell>
            <TableCell>Auteur</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {posts && posts.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{new Intl.DateTimeFormat('fr-FR').format(new Date(row.date))}</TableCell>
              <TableCell>{<strong>{row.titre}</strong>}</TableCell>
              <TableCell>{row.texte.substr(0, 40)}</TableCell>
              <TableCell align="right">{row.auteur}</TableCell>

              <CardActions>
                {/* <Button  onClick={() => update(row.id)} variant="outlined" color="primary">
                  <Link to={"/edit/" + row.id}>Edit</Link>
                </Button> */}
                <Button onClick={() => onDelete(row.id)} variant="outlined" color="primary">Delete
                </Button>
              </CardActions>

            </TableRow>
          ))}
        </TableBody>
      </Table>
      {/* <div className={classes.seeMore}>
        <Link color="primary" href="#">
          Voir plus d'articles
        </Link>
      </div> */}
    </React.Fragment>
  );
}
