import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';
import moment from "moment";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

export default function Deposits() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Articles</Title>
      <Typography component="p" variant="h4">
        DERNIER  ARTICLE
      </Typography>
      <Typography color="textSecondary" className={classes.depositContext}>
      {new Date().toLocaleString() + ""}
      </Typography>
      <div>
        <Link color="primary" href="#" onClick={preventDefault}>
          Voir article
        </Link>
      </div>
    </React.Fragment>
  );
}