import React, { useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Header from './Header';
import FeaturedPost from './FeaturedPost';
import Footer from './Footer';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPosts } from '../../stores/post-slice';


const sections = [
  { title: 'Guide', url: '#' },
  { title: 'Conseils', url: '#' },
  { title: 'Destinations', url: '#' }

];


export default function Blog() {

  const dispatch = useDispatch()
  const posts = useSelector(state => state.posts.list);

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);


  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Blog" sections={sections} />
        <main>
          <Grid container spacing={4}>
            {posts.map((post) => (
              <FeaturedPost key={post.title} post={post} />
            ))}
          </Grid>
        </main>
      </Container>
      <Footer />
    </React.Fragment>
  );
}