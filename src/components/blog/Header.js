import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { Link as Attach } from "react-router-dom";
import BadgeAvatars from './Badge';
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../stores/auth-slice";
import { Hidden } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto',
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const dispatch = useDispatch();



  //use of the useSelector to retrieve the value of the user from the store
  const user = useSelector(state => state.auth.user);
  const { sections, title } = props;

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>


      {/* Using a condition with the ternary operator to display the buttons according to the state of my User */}

        {user ?
          ''
          :

          <Attach to="/login"><Button variant="outlined" size="small">Login</Button></Attach>}


        {user &&
          <>

            <ul><li><BadgeAvatars /></li>
              <li>Bienvenue {user.email}</li>

            </ul>
            <Attach to="/homeboard"><Button variant="outlined" size="small">Dashboard</Button></Attach>
          </>}
        <Hidden smDown>
          <Typography
            component="h2"
            variant="h5"
            color="inherit"
            align="center"
            noWrap
            className={classes.toolbarTitle}
          >
            {title} Voyage
          </Typography>
        </Hidden>

        {user ?
          <Button variant="outlined" size="small" onClick={() => dispatch(logout())}>
            Logout </Button>
          :
          <Attach to="/signup"><Button variant="outlined" size="small">Sign up</Button></Attach>}
      </Toolbar>

      <Hidden mdUp>
        <Toolbar className={classes.toolbar}>
          <Typography
            component="h2"
            variant="h5"
            color="inherit"
            align="center"
            noWrap
            className={classes.toolbarTitle}
          >
            {title} Voyage
          </Typography>
        </Toolbar>
      </Hidden>


      <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
        {sections.map((section) => (
          <Link
            color="inherit"
            noWrap
            key={section.title}
            variant="body2"
            href={section.url}
            className={classes.toolbarLink}
          >
            {section.title}
          </Link>
        ))}
      </Toolbar>
    </React.Fragment>
  );
}

Header.propTypes = {
  sections: PropTypes.array,
  title: PropTypes.string,
};

