import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});

export default function FeaturedPost(props) {
  const classes = useStyles();
  const { post } = props;

  return (
    <Grid item xs={12} md={6}>
      <CardActionArea component="a" href="#">
        <Card className={classes.card}>
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                {post.titre}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {new Intl.DateTimeFormat('fr-FR').format(new Date(post.date))}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {post.auteur}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                {post.texte.substr(0, 100)}
              </Typography>
              <Link to={'/' + post.id}>
                <Typography variant="subtitle1" color="primary">
                  Lire la suite...
                </Typography>
              </Link>
            </CardContent>
          </div>
          <Hidden>
            <CardMedia className={classes.cardMedia}
            component="img"
            image={post.picture.startsWith('http') ? post.picture : process.env.REACT_APP_SERVER_URL + post.picture}
            title={post.imageTitle}
            alt={post.name}
          />
          </Hidden>
        </Card>
      </CardActionArea>
    </Grid>
  );
}

FeaturedPost.propTypes = {
  post: PropTypes.object,
};