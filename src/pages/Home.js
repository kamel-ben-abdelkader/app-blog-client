import React from "react";
import { useDispatch } from "react-redux";
import Blog from "../components/blog/Blog";
import { fetchPosts } from "../stores/post-slice";


export function Home() {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);

  // const dispatch = useDispatch()
  // dispatch(fetchPosts());
  return (

    <div>
      <Blog />
    </div>
  )
}
