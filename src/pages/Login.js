import { useState } from "react";
import { LoginForm } from "../components/LoginForm";
import { AuthService } from "../services/AuthService";
import { useDispatch } from "react-redux";
import { login } from "../stores/auth-slice";
import { useHistory } from "react-router-dom";




export function Login() {
    let history = useHistory();
    function handleClick() {
        history.push("/homeboard");
    }


    const dispatch = useDispatch();
    const [feedback, setFeedback] = useState('');
    const loginRequest = async (credentials) => {
        try {
            const user = await AuthService.login(credentials);
            dispatch(login(user));
            setFeedback('Hello ' + user.email)
            handleClick()


        } catch (error) {
            setFeedback('Credentials error');
        }

    };

    return (

        <div>
            {feedback}
            <LoginForm onFormSubmit={loginRequest} />
        </div>
    )
}



