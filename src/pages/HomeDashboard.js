import React, { useEffect } from "react";
import Dashboard from "../components/dashboard/Dashboard";
import { useDispatch } from "react-redux";
import { fetchPosts } from "../stores/post-slice";



export function HomeDashboard() {

  const dispatch = useDispatch()
  // const posts = useSelector(state => state.posts.listUser);

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);

  return (
    <div>
      <Dashboard />
      {/* <PostForm onSubmit={(post) => dispatch(addOnePosts(post))} /> */}
    </div>
  )
}
