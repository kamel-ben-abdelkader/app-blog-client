
import { useState } from "react";
import { AuthService } from "../services/AuthService";
import { SignUpForm } from "../components/SignUpForm";
import { useHistory } from "react-router-dom";


export function SignUp() {
    const [feedback, setFeedback] = useState('');
    let history = useHistory();
    function handleClick() {
        history.push("/login");
    }

    const registerRequest = async (user) => {
        try {

            await AuthService.register(user);
            setFeedback('Registration successful');
            handleClick()
        } catch (error) {


            setFeedback(error.response.data.error);
        }

    }

    return (
        <div>
            {feedback}
            <SignUpForm onFormSubmit={registerRequest} />
        </div>
    )
}
