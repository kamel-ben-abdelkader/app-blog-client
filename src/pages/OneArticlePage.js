import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom"
import { fetchOneArticle } from "../stores/post-slice";
import Paper from '@material-ui/core/Paper';
import { Button } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));

export function OneArticlePage() {
    const classes = useStyles();
    const { id } = useParams();
    const article = useSelector(state => state.posts.oneArticle);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchOneArticle(Number(id)));
    }, [dispatch]);


    if (!article) {
        return <div>Loading...</div>
    }

    return (

        <div className={classes.root}>
            <Typography component="h2" variant="h5">
                <Link to="/"><Button variant="outlined" size="large" color='primary' >Home</Button></Link>
            </Typography>
            <Paper className={classes.paper}>


                <Grid container spacing={2}>
                    <Grid item>


                        <Card>
                            <Hidden>
                                <CardMedia component="img"
                                    image={article.picture.startsWith('http') ? article.picture : process.env.REACT_APP_SERVER_URL + article.picture}
                                    title={article.imageTitle}
                                    alt={article.titre} />
                            </Hidden>
                            <div>
                                <CardContent>
                                    <Typography component="h2" variant="h5">
                                        {article.titre}
                                    </Typography>
                                    <Typography variant="subtitle1" color="textSecondary">
                                        {new Intl.DateTimeFormat('fr-FR').format(new Date(article.date))}
                                    </Typography>
                                    <Typography variant="subtitle1" color="textSecondary">
                                        {article.auteur}
                                    </Typography>
                                    <Typography variant="subtitle1" paragraph>
                                        {article.texte}
                                    </Typography>

                                </CardContent>
                            </div>

                        </Card>


                    </Grid>
                </Grid>
            </Paper>

        </div>
    );
}
