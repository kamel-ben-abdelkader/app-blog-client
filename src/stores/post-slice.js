import { createSlice } from "@reduxjs/toolkit";
import { PostService } from "../services/PostService";


const postSlice = createSlice({
    name: 'posts',
    initialState: {
        list: [],
        listUser: [],
        oneArticle: null
        // selected: []
    },
    reducers: {
        setList(state, { payload }) {
            state.list = payload;
        },
        setListUser(state, { payload }) {
            state.listUser = payload;
        },
        addPost(state, { payload }) {
            state.listUser.push(payload);
        },
        editPost(state, { payload }) {
            state.listUser = payload;
        },
        setOneArticle(state, {payload}) {
            state.oneArticle = payload;
        }

        // toggleSelectPost(state, {payload}) {

        //     let index = state.selected.findIndex(item => item.id === payload.id)

        //     if (index !== -1) {


        //     state.selected.splice (index, 1)
        //         console.log("index", index);
        //     }
        // else {
        //     state.selected.push(payload)
        //      }
        // },

        // clearSelectedPost(state){
        //     state.selected = [];


        // }
    }
});

export const { addPost, setList, setListUser,setOneArticle } = postSlice.actions;

export default postSlice.reducer;

export const fetchPosts = () => async (dispatch) => {
    try {
        const response = await PostService.fetchAllPost();
        dispatch(setList(response));
    } catch (error) {
        console.log(error);
    }
}

export const addOnePosts = (post) => async (dispatch) => {

    try {
        const response = await PostService.add(post);
        dispatch(addPost(response));
    } catch (error) {
        console.log(error);
    }
}

export const fetchPostsByUser = (userId) => async (dispatch) => {
    try {
        const response = await PostService.fetchAllPostByUser(userId);
        dispatch(setListUser(response));
    } catch (error) {
        console.log(error);
    }
}

export const deletePosts = (id) => async(dispatch) => {

    try {
        const response = await PostService.deleteMyPost(id);   

    } catch (error) {
        console.log(error)
    }
} 


export const updatePosts = (id) => async(dispatch) => {

    try {
        const response = await PostService.updateMyPost(id);
   
        dispatch(setListUser(response));
    } catch (error) {
        console.log(error)
    }
} 

export const fetchOneArticle = (id) => async (dispatch) => {
    try {

        const response = await PostService.findOnePost(id);

        
        dispatch(setOneArticle(response));
    } catch (error) {
        console.log(error);
    }
}