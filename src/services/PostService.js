import axios from "axios";



export class PostService {


    static async fetchAllPost() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post');
        
        return response.data;
    }

    static async fetchAllPostByUser(userId) {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/user/' + userId);
        return  response.data;
      
    }


    static async deleteMyPost(id) {
        const response =  await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/post/' + id)
        return  response.data;
      
    }

    static async add(post) {

        const formData = new FormData();
        formData.append('date',    post.date);
        formData.append('auteur',  post.auteur);
        formData.append('titre',   post.titre);
        formData.append('texte',   post.texte);
        formData.append('picture', post.picture);

        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/post', formData);
        return response.data;
    }

    static async updateMyPost(id) {
        const response =  await axios.patch(process.env.REACT_APP_SERVER_URL+'/api/post/' + id)
        return  response.data;
      
    }

    static async findOnePost(id) {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/' + id);
        return  response.data;
      
    }
 

}
