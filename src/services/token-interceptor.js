import axios from "axios";

/** 
 * Use of axios interceptors to add the token in the headers (if it exists)
 */
axios.interceptors.request.use((config) => {
    const token =localStorage.getItem('token');
    if(token) {

        config.headers.authorization = 'bearer '+token;
    }
    return config;
});
