
import './App.css';
import { Home } from './pages/Home';
import { Route, Switch } from "react-router-dom";
import { Login } from "./pages/Login";
import { SignUp } from "./pages/SignUp";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { loginWithToken } from "./stores/auth-slice";
import { HomeDashboard } from './pages/HomeDashboard';
import { ProtectedRoute } from './components/ProtectedRoute';
import { PostForm } from './components/PostForm';
import { addOnePosts } from './stores/post-slice';
import { OneArticlePage } from './pages/OneArticlePage';



function App() {


  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loginWithToken());
  }, [dispatch]);


  return (
    <div>
      <Switch>
        <Route path="/signup">
          <SignUp />
        </Route>

        <Route path="/login">
          <Login />
        </Route>

        <Route path="/" exact>
          <Home />
        </Route>


        <ProtectedRoute path="/homeboard">
          <HomeDashboard />
        </ProtectedRoute>

        <ProtectedRoute path="/add-article">
          <PostForm onSubmit={(post) => dispatch(addOnePosts(post))} />
        </ProtectedRoute>

        <Route path="/:id">
          <OneArticlePage />
        </Route>   

        {/*
         //In progress for the improved version with editing of articles 
         <ProtectedRoute path="/edit/:id">
        <EditArticlePage />
        </ProtectedRoute>      */}

      </Switch>
    </div>
  );
}

export default App;
